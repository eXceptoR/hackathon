from flask import session, request, g
from chatengine import app
from models import User


@app.before_request
def before_request():
    g.user = None

    if 'user_data' in session and session.get('user_data'):
        try:
            g.user = User.objects.get(github_user_id=session.get('user_data').get('github_user_id'))
            if request.path and not request.path.startswith('/static'):
                print '{0}Request by user {1}'.format(
                    '' if 'api' not in request.path else 'API ', g.user
                )
        except:
            import sys
            print sys.exc_info()
            del session['user_data']


@app.after_request
def after_request(response):
    response.headers['Server'] = 'Finely trained slowpokes v1.25'
    response.headers['Access-Control-Allow-Origin'] = '*'

    return response
