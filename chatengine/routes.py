from flask import session, redirect, render_template, url_for
from chatengine import app
from .github_connector import github
from models import Channel


@app.route('/')
def index():
    if session.get('user_data'):
        return redirect(url_for('channels'))
    else:
        return render_template('login.html', angular_controller='MainCtrl')


@app.route('/login')
def login():
    return github.authorize() if not session.get('user_data') else redirect(url_for('spa'))


@app.route('/channels')
def channels():
    if not session.get('user_data'):
        return redirect(url_for('index'))

    return render_template('channels.html', channels=Channel.objects(),
                           angular_controller='ChannelCtrl')


@app.route('/logout')
def logout():
    try:
        del session['user_data']
    except:
        pass

    return redirect(url_for('index'))
