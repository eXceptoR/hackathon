var app = angular.module('chat', ['ngMaterial']);

app.config(['$interpolateProvider', '$mdThemingProvider',
    function ($interpolateProvider, $mdThemingProvider) {
        $mdThemingProvider.theme('default').primaryPalette('light-blue', {
            'default': '400'
        })
            .accentPalette('deep-orange', {
                'default': '500'
            });
        $interpolateProvider.startSymbol('{[');
        $interpolateProvider.endSymbol(']}');
    }
]);

app.controller('MainCtrl', ['$scope', '$window', function ($scope, $window) {
    $scope.doLogin = function () {
        $window.location.href = '/login';
    }
}]);


app.controller('ChannelCtrl', function ($scope, $timeout, $mdSidenav, $log, $http, $mdDialog, $window) {
    var socket = io.connect('http://' + document.domain + ':' + location.port + '/chat');
    $scope.messages = {};
    $scope.unread = {};

    socket.on('connect', function () {
        // TODO: lock UI and unlock when connected
        console.log('I am connected');
        $scope.get_channels(true);
    });

    $scope.showJoinChannel = function (ev) {
        var confirm = $mdDialog.confirm()
            .title('Join Channel?')
            .textContent('You are going to join channel #' + ev.name)
            .ok('OK')
            .cancel('No, thanks');
        $mdDialog.show(confirm).then(function () {
            $scope.join(ev.name, false);
            $scope.get_channels(); // to rebuild channel list at right
            $scope.channel = ev.name;
        }, function () {
            // on cancel - do nothing
        });
    };

    $scope.get_channels = function (init) {
        $http.get('http://' + document.domain + ':' + location.port + '/api/channel').
            success(function (data) {
                $scope.channels = data;
                if (init) {
                    angular.forEach(data.subscribed, function (value, key) {
                        $scope.messages[value.name] = [];
                        $scope.unread[value.name] = 0;
                    })
                }
            });
    };

    $scope.get_unread_count = function (channel) {
        var data = $scope.unread[channel];
        return (data > 0) ? data : null;
    };

    $scope.join = function (channel_name, switch_channel) {
        socket.emit('join channel', {channel_name: channel_name});
        if (switch_channel) {
            $scope.channel = channel_name;
        }
        $scope.unread[channel_name] = 0;
        // TODO: API call to server, load last X messages
    };

    $scope.leave = function (channel_name) {
        socket.emit('leave channel', {channel_name: channel_name});
    };

    $scope.showLeaveChannel = function (channel_name) {
        var confirm = $mdDialog.confirm()
            .title('Leave Channel?')
            .textContent('You are going to leave channel #' + channel_name)
            .ok('OK')
            .cancel('No, thanks');
        $mdDialog.show(confirm).then(function () {
            $scope.leave(channel_name);
            $scope.get_channels(); // to rebuild channel list at right
            $scope.channel = null;
        }, function () {
            // on cancel - do nothing
        });
    };


    $scope.doLogout = function () {
        $window.location.href = '/logout';
    };

    $scope.sendMessage = function (channel, msg) {
        console.log("Sending from channel " + channel + " msg: " + msg);
        socket.emit('message', {
            channel: channel,
            message: msg
        });

        $scope.message = null;
    };


    socket.on('message', function (msg) {
        console.log("Received from server:");
        console.log(msg);

        // msg received for current screen, need to redraw it
        $scope.messages[msg.channel].push(msg);

        if ($scope.channel == msg.channel) {
            var c = document.getElementById('chatwindow');
            c.scrollTop = c.scrollHeight;
        } else {
            $scope.unread[msg.channel] = $scope.unread[msg.channel] + 1;
        }

        $scope.$apply();

    });

    $scope.toggleLeft = buildDelayedToggler('left');
    $scope.toggleRight = buildToggler('right');
    $scope.isOpenRight = function () {
        return $mdSidenav('right').isOpen();
    };
    /**
     * Supplies a function that will continue to operate until the
     * time is up.
     */
    function debounce(func, wait, context) {
        var timer;
        return function debounced() {
            var context = $scope,
                args = Array.prototype.slice.call(arguments);
            $timeout.cancel(timer);
            timer = $timeout(function () {
                timer = undefined;
                func.apply(context, args);
            }, wait || 10);
        };
    }

    /**
     * Build handler to open/close a SideNav; when animation finishes
     * report completion in console
     */
    function buildDelayedToggler(navID) {
        return debounce(function () {
            $mdSidenav(navID)
                .toggle()
                .then(function () {
                    $log.debug("toggle " + navID + " is done");
                });
        }, 200);
    }

    function buildToggler(navID) {
        return function () {
            $mdSidenav(navID)
                .toggle()
                .then(function () {
                    $log.debug("toggle " + navID + " is done");
                });
        }
    }

});

app.controller('LeftCtrl', function ($scope, $timeout, $mdSidenav, $log) {
    $scope.close = function () {
        $mdSidenav('left').close()
            .then(function () {
                $log.debug("close LEFT is done");
            });
    };
});

app.controller('RightCtrl', function ($scope, $timeout, $mdSidenav, $log) {
    $scope.close = function () {
        $mdSidenav('right').close()
            .then(function () {
                $log.debug("close RIGHT is done");
            });
    }
});
