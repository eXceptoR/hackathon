from flask import session, redirect, url_for
from mongoengine import DoesNotExist
from flask.ext.github import GitHub
from chatengine import app
from models import User

github = GitHub(app)


@github.access_token_getter
def token_getter():
    user = session.get('user_data')
    if user is not None:
        return user.github_access_token


@app.route('/github-callback')
@github.authorized_handler
def authorized(access_token):
    print 'access_token!', access_token
    if access_token is None:
        return redirect(url_for('login'))

    modified = False
    metadata = github.get('user', access_token=access_token)
    github_uid = metadata.get('id')

    if not github_uid:
        # TODO error log
        print 'failed to get gitbub uid'
        return redirect(url_for('index'))

    try:
        user = User.objects.get(github_user_id=github_uid)
    except DoesNotExist:
        user = User(github_user_id=github_uid)
        modified = True

    session['user_data'] = user.to_dict()

    if user and user.metadata != metadata:
        user.metadata = metadata
        modified = True

    if modified:
        user.save()

    print 'saved session...'
    response = redirect(url_for('channels'))
    response.set_cookie('github_user_id', str(user.github_user_id))
    return response
