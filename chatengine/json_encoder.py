from flask.json import JSONEncoder
from datetime import datetime, timedelta


class CustomJSONEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime):
            return obj.isoformat()
        elif isinstance(obj, timedelta):
            return (datetime.min + obj).time().isoformat()
        elif isinstance(obj, set):
            return list(obj)
        else:
            try:
                return super(CustomJSONEncoder, self).default(obj)
            except TypeError:
                return "<not serializable: %s>" % str(type(obj))
