#!/usr/bin/env python

from gevent import monkey

from flask import Flask
from flask.ext.mongoengine import MongoEngine
from flask_socketio import SocketIO
from flask_restful import Api
from json_encoder import CustomJSONEncoder

monkey.patch_all()

app = Flask(__name__)
api = Api(app, prefix='/api')
app.json_encoder = CustomJSONEncoder
app.config.from_object('chatengine.settings')

socketio = SocketIO(app, async_mode='gevent')
db = MongoEngine(app)

from . import github_connector
from . import app_common
from . import models
from . import api
from . import routes
from . import socket_handlers
