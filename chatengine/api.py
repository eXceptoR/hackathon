import json
from functools import wraps
from flask import make_response, session
from chatengine import api
from json_encoder import CustomJSONEncoder
from flask_restful import Resource, abort as api_abort
from models import Channel, User


@api.representation('application/json')
def output_json(data, code, headers=None):
    resp = make_response(
        json.dumps(data, cls=CustomJSONEncoder, indent=4),
        code
    )
    resp.headers.extend(headers or {})
    return resp


def authenticate(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if not getattr(func, 'authenticated', True):
            return func(*args, **kwargs)

        if session.get('user_data'):
            return func(*args, **kwargs)

        api_abort(401)

    return wrapper


class Main(Resource):
    method_decorators = [authenticate]

    def get(self):
        return {
            'versions': {
                'values': [
                    {
                        'id': 'v1',
                        'status': 'CURRENT'
                    }
                ]
            }
        }


class ChannelResource(Resource):
    method_decorators = [authenticate]

    def get(self):
        request_user = User.get_by_github_id(session.get('user_data').get('github_user_id'))
        data = {
            'subscribed': [],
            'unsubscribed': []
        }

        for channel in Channel.objects():
            channel_dict = channel.to_dict()
            target = 'subscribed' if request_user in channel.users else 'unsubscribed'
            data[target].append(channel_dict)

        for x in ['subscribed', 'unsubscribed']:
            data[x] = sorted(data[x], key=lambda x: x.get('name'))

        return data

    def post(self):
        # subscribe to channel
        pass

    def delete(self):
        # unsubscribe
        pass


api.add_resource(Main, '/')
api.add_resource(ChannelResource, '/channel')
