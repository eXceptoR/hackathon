from flask import session
from flask_socketio import emit, join_room, leave_room
from chatengine import socketio
from models import User, Channel


@socketio.on('connect', namespace='/chat')
def socket_connect():
    _joined = []
    user = User.get_by_github_id(session.get('user_data').get('github_user_id'))
    for channel in Channel.objects():
        if user in channel.users:
            join_room(channel.name)
            _joined.append(channel.name)

    print 'Connected user %s (auto joined channels: %s)' % (user, _joined)


@socketio.on('disconnect', namespace='/chat')
def socket_disconnect():
    print('Client disconnected')


@socketio.on('join channel', namespace='/chat')
def socket_join_channel(message):
    user = User.get_by_github_id(session.get('user_data').get('github_user_id'))
    join_room(message['channel_name'])
    channel = Channel.objects.get(name=message.get('channel_name'))
    if user not in channel.users:
        channel.users.append(user)
        channel.save()
        print 'User %s stored channel %s in subscriptions' % (user, message.get('channel_name'))

    print 'User %s joined channel %s' % (user, message.get('channel_name'))


@socketio.on('leave channel', namespace='/chat')
def socket_leave_channel(message):
    user = User.get_by_github_id(session.get('user_data').get('github_user_id'))
    leave_room(message.get('channel_name'))
    channel = Channel.objects.get(name=message.get('channel_name'))
    if user in channel.users:
        channel.users.remove(user)
        channel.save()

    print 'User %s unsubscribed from %s ' % (user, message.get('channel_name'))


class ChatMessage(object):
    def __init__(self, raw_msg):
        self.message = raw_msg.get('message')
        self.channel = raw_msg.get('channel')

    def validate(self):
        if not self.message or not self.channel:
            return

        if len(self.message) > 100:
            self.message = self.message[:100] = "(...cut)"

        return True

    def save(self):
        # TODO: save message
        pass


@socketio.on('message', namespace='/chat')
def socket_message(message):
    print 'msg recvd: %s' % message
    print 'origin:', session.get('user_data')

    msg = ChatMessage(message)

    if msg.validate():
        emit(
            'message',
            {
                'channel': msg.channel,
                'message': msg.message,
                'user': session.get('user_data')
            },
            room=msg.channel
        )
        msg.save()
    else:
        print 'validation failed'
