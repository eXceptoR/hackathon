# coding: utf-8

from datetime import datetime
from mongoengine import StringField, DictField, IntField, DateTimeField, ListField, ReferenceField
from chatengine import db
from settings import DEFAULT_AVATAR


class User(db.Document):
    github_user_id = IntField()
    metadata = DictField()
    registered = DateTimeField(default=datetime.utcnow())

    def to_dict(self):
        return {
            'github_user_id': self.github_user_id,
            'metadata': self.metadata
        }

    @classmethod
    def get_by_github_id(cls, github_id):
        return cls.objects.get(github_user_id=github_id)

    def __unicode__(self):
        return '<DB: Github user: {0}>'.format(self.metadata.get('login'))


class Channel(db.Document):
    name = StringField()
    users = ListField(ReferenceField(User))
    description = StringField()
    avatar_url = StringField()

    def __unicode__(self):
        return '<DB: Channel #{0}>'.format(self.name)

    def to_dict(self):
        data = {
            'name': self.name,
            'description': self.description,
            'avatar_url': self.avatar_url or DEFAULT_AVATAR
        }

        return data
